package main

import (
	"os"

	"github.com/go-vgo/robotgo"
)

func main() {
	var f *os.File
	var err error

	if len(os.Args) > 1 {
		f, err = os.Open(os.Args[1])
		if err != nil {
			panic(err)
		}
	} else {
		f = os.Stdin
	}

	var size int
	chunk := make([]byte, 255)

	read := func() bool {
		n, err := f.Read(chunk[:])
		size = n
		return (err == nil && n > 0)
	}

	robotgo.Sleep(2)

	for read() {
		// write
		robotgo.TypeStr(string(chunk[:size]))
		robotgo.Sleep(1)
	}
}
